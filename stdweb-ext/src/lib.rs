#![feature(custom_attribute)]
#![recursion_limit="500"]

#[macro_use]
extern crate stdweb;
#[macro_use]
extern crate stdweb_derive;
extern crate unwrap;

use stdweb::Value;
use stdweb::Reference;
use stdweb::web::Window;
use stdweb::web::IWindowOrWorker;
use stdweb::web::IEventTarget;
use stdweb::web::event::IMessageEvent;
use stdweb::web::event::IEvent;
use stdweb::serde::ConversionError;

use unwrap::unwrap;

// Unstable bits
use stdweb::unstable::TryInto;

//#[reference(subclass_of(Event))]
#[derive(Clone, Debug, PartialEq, Eq, ReferenceType)]
#[reference(instance_of = "MessageEvent")]
#[reference(event = "message")]
pub struct WorkerMessageEvent( Reference );

impl IEvent for WorkerMessageEvent {}

impl IMessageEvent for WorkerMessageEvent {
  type Data = Value;
}

#[derive(Clone, Debug, PartialEq, Eq, ReferenceType)]
#[reference(instance_of = "Worker")]
pub struct Worker( Reference );

impl Worker {
  pub fn new(a: String) -> Self {
    unwrap!(js! { return new Worker(@{a});  }.try_into())
  }

  pub fn post_message<T>(&self, msg: T) where T: TryInto<Value>,
      <T as TryInto<stdweb::Value>>::Error : std::fmt::Debug {
    js! { @(no_return) @{self}.postMessage(@{unwrap!(msg.try_into())}) };
  }
}

pub fn post_message<T>(msg: T) where T: TryInto<Value>, <T as TryInto<stdweb::Value>>::Error : std::fmt::Debug {
  js! { @(no_return) postMessage(@{unwrap!(msg.try_into())})  };
}

impl IEventTarget for Worker {}

pub trait IWorkerGlobalScope : IWindowOrWorker {

}

#[derive(Clone, Debug, PartialEq, Eq, ReferenceType)]
#[reference(instance_of = "WorkerGlobalScope")]
pub struct WorkerGlobalScope( Reference );
impl IWindowOrWorker for WorkerGlobalScope {}
impl IWorkerGlobalScope for WorkerGlobalScope {}
impl IEventTarget for WorkerGlobalScope {}

pub fn worker() -> WorkerGlobalScope {
  unwrap!(js! { return self;  }.try_into())
}
