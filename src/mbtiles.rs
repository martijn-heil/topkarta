/*
  TopKarta, a map viewer.
  Copyright (C) 2018 Martijn Heil

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::rc::*;
use std::cell::*;

use yew::prelude::worker::*;
use yew::prelude::*;

use stdweb::web::*;
use stdweb::PromiseFuture;
use stdweb::Value;
use stdweb::serde::ConversionError;
use stdweb::traits::IMessageEvent;
use stdweb::web::event::ConcreteEvent;

use unwrap::unwrap;
use unwrap::unwrap_err;

use log::{trace, debug, info, warn, error};

use topkarta_dataworker::Request;
use topkarta_dataworker::Response;

use stdweb_ext::*;


// NOTE Unstable bits:

// Will be replaced by Rust's std TryFrom/TryInto
use stdweb::unstable::TryInto;
use stdweb::unstable::TryFrom;

// Currently in alpha, will probably change quite a bit and be merged someday into Rust stable
// We really need it in the browser environment however, so we don't have much choice other than to
// use it.
use futures::Poll;
use futures::future::poll_fn;
use futures::prelude::*;

use stdweb::spawn_local;
use stdweb::web::wait;

// End of unstable bits

/*
 * This wraps a MBTiles database (which is really a SQLite database with further specified contents).
 * It provides read-only access to the MBTiles database.
 *
 * TODO General purpose SQLite database worker abstraction which the MBTiles thing can be built
 * upon.
 */
pub struct MBTiles {
  blob: Blob,
  name: String
}

async fn repetitively_poll_fn<T, F>(mut f: F) where F: FnMut() -> Poll<T> {
  await!(poll_fn(move |waker| {
    let state = f();
    if let Poll::Pending = state {
      let w = waker.clone();
      set_timeout(move || w.wake(), 1);
    }
    state
  }));
}

/*
 * For whatever reason, async fn does not seem to work in impl blocks.
 * For that reason I seperated the async functions.
 * I guess (hope?) this will be resolved when async/await and Futures become stable.
 * When that happens we can refactor this.
 */

async fn mbtiles_new(blob: Blob, name: String) -> MBTiles {
  console!(log, "in mbtiles_new");
  let path = format!("/{}", name);

  debug!("Creating data worker..");
  let worker = Worker::new(String::from("topkarta-dataworker.js"));
  await!(wait(3000));
  let state: Rc<Cell<Poll<()>>> = Rc::new(Cell::new(Poll::Pending));
  debug!("Sending initialization request to worker..");
  worker.post_message(Request::Init(name.clone(), path, js! { return @{&blob};  }));
  let state2 = Rc::clone(&state);
  worker.add_event_listener(move |e: WorkerMessageEvent| {
    let resp: Response = unwrap!(e.data().try_into());
    if let Response::Ready(_) = resp { debug!("Setting state.."); state2.set(Poll::Ready(()));  }
  });

  // TODO: Can we ignore the argument like this, or are we supposed to do something with it? Need to figure that out
  debug!("Entering await of poll_fn..");
  await!(poll_fn(|waker| {
    let s  = state.get();
    debug!("polled! {:?}", &s);
    if let Poll::Pending = s {
      let w = waker.clone();
      set_timeout(move || w.wake(), 1);
    }
    s
  }));
  // TODO this breaks the nightly compiler apparently, somehow
  //await!(repetitively_poll_fn(|| state.get()));

  debug!("MBTiles data worker class initialization fully completed, returning..");
  MBTiles { blob: blob, name: name }
}

async fn mbtiles_get_tile(me: &MBTiles, z: u32, x: u32, y: u32) {

}

impl MBTiles {
  pub fn new(blob: Blob, name: String) -> impl Future<Output = Self> {
    debug!("in mbtiles new impl");
    mbtiles_new(blob, name)
  }

  fn get_tile(&self, z: u32, x: u32, y: u32) {
    mbtiles_get_tile(self, z, x, y);
  }
}

async fn await_event<T, F>(target: EventTarget, filter: F) where F: Fn() -> bool, T: ConcreteEvent {
  let mut state = Poll::Pending;
  target.add_event_listener(move |e: T| {
    state = Poll::Ready(());
  });
  await!(poll_fn(|_| state.clone()))
}
