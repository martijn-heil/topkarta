/*
  TopKarta, a map viewer.
  Copyright (C) 2018  Martijn Heil

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#![recursion_limit="500"]
#![feature(async_await, futures_api, await_macro, pin)]
#![feature(try_from)]
#![feature(custom_attribute)]

#[macro_use]
extern crate yew;
extern crate rand;
#[macro_use]
extern crate stdweb;
extern crate unwrap;
extern crate uuid;
extern crate futures;
extern crate stdweb_ext;
extern crate log;

#[macro_use]
extern crate stdweb_derive;

extern crate topkarta_dataworker;

pub mod components;
pub mod mbtiles;

use yew::prelude::*;

pub struct Model {
  test: u32,
}

pub enum Msg {
  Click,
}

impl Component for Model {
  type Message = Msg;
  type Properties = ();

  fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
    Model { test: 0 }
  }

  fn update(&mut self, msg: Self::Message) -> ShouldRender {
    match msg {
      Msg::Click => {
        self.test = rand::random();
      }
    }
    true
  }
}

impl Renderable<Model> for Model {
  fn view(&self) -> Html<Self> {
    html! {
      <div>
        <button onclick=|_| Msg::Click,>{ self.test }</button>
      </div>
    }
  }
}
