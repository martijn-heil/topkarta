/*
  TopKarta, a map viewer.
  Copyright (C) 2018  Martijn Heil

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::rc::*;
use std::cell::*;

use stdweb::spawn_local;
use stdweb::web::*;
use stdweb::traits::IDragEvent;
use stdweb::traits::IEvent;
use stdweb::web::event::DataTransferItemKind;
use stdweb::Value;
use yew::prelude::*;
use yew::{html, html_impl};
use uuid::Uuid;
use unwrap::unwrap;
use components;
use mbtiles;
use mbtiles::MBTiles;
use log::{trace, debug, info, warn, error};

// Unstable bits
use stdweb::unstable::TryInto;
use stdweb::unstable::TryFrom;

use futures::future::{self, FutureExt, Future};


type Map = components::map::Model;

pub struct Model {
  id: String, // Note: this includes a hashtag id selector, which is not really part of the HTML ID, the HTML ID is string slice &id[1..]
  link: ComponentLink<Model>,
  mbtiles: Rc<Cell<Option<MBTiles>>>,
}

pub enum Msg {
  FileUploaderSubmitted(ChangeData),
  FileUploaded(FileList),
  SomethingWasDropped(DragDropEvent),
  DragOver(DragOverEvent),
  DragEnter(DragEnterEvent),
  DragEnd(DragEndEvent),
  Drag(DragEvent),
  DragExit(DragExitEvent),
  DragLeave(DragLeaveEvent),
}

impl Component for Model {
  type Properties = ();
  type Message = Msg;

  fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
    Model { id: format!("#a{}", uuid::adapter::Simple::from_uuid(Uuid::new_v4())), link: link, mbtiles: Rc::new(Cell::new(None)) }
  }

  fn update(&mut self, msg: Self::Message) -> ShouldRender {
    match msg {
      Msg::FileUploaderSubmitted(cd) => {
        let maybe = unwrap!(document().query_selector(&self.id));
        let definitely = unwrap!(maybe);
        let files: FileList = unwrap!(stdweb::unstable::TryInto::try_into(js!(return @{definitely}.files)));
        self.link.send_self(Msg::FileUploaded(files));
        false
      }

      Msg::FileUploaded(files) => {
        console!(log, files);
        false
      }

      Msg::SomethingWasDropped(event) => {
        event.prevent_default();
        let maybe_data_transfer = event.data_transfer();
        if let None = maybe_data_transfer { return false }
        let data_transfer = maybe_data_transfer.unwrap();
        for f in data_transfer.items().iter().filter(|i| i.kind() == DataTransferItemKind::File).map(|f| unwrap!(f.get_as_file())) {
          println!("received a file!");
          console!(log, &f);
          let storage_location = Rc::downgrade(&self.mbtiles);
          let fut = MBTiles::new(unwrap!(f.as_ref().try_into()), f.name())
            .then(move |res| {
                if let Some(reference) = storage_location.upgrade() {
                  reference.set(Some(res));
                }
                console!(log, "done initialization!");
                future::ready(())
            } );

          debug!("spawn_local...");
          spawn_local(fut);
         }
        false
      }

      Msg::DragOver(event) => { event.prevent_default(); false }
      Msg::DragEnter(event) => { event.prevent_default(); false }
      Msg::DragEnd(event) => { event.prevent_default(); false }
      Msg::Drag(event) => { event.prevent_default(); false }
      Msg::DragExit(event) => { event.prevent_default(); false }
      Msg::DragLeave(event) => { event.prevent_default(); false }
    }
  }
}

impl Renderable<Model> for Model {
  fn view(&self) -> Html<Self> {
    html! {
      <div id="topkarta--components--app",
        ondrop=|e| Msg::SomethingWasDropped(e),
        ondragover=|e| Msg::DragOver(e),
        ondragenter=|e| Msg::DragEnter(e),
        ondragend=|e| Msg::DragEnd(e),
        ondrag=|e| Msg::Drag(e),
        ondragexit=|e| Msg::DragExit(e),
        ondragleave=|e| Msg::DragLeave(e),
      >
        // The range is to exclude the first # character
        <input id={&self.id[1..]}, type="file", onchange=|cd| Msg::FileUploaderSubmitted(cd),/>
        <Map/>
      <div/>
    }
  }
}
