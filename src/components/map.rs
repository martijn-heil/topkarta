/*
  TopKarta, a map viewer.
  Copyright (C) 2018  Martijn Heil

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use yew::prelude::*;
use yew::virtual_dom::VNode;

use stdweb::Value;
use stdweb::web::Node;
use stdweb::unstable::TryFrom;
use stdweb::unstable::TryInto;


pub struct Model {
  map: Value,
  container: Value,
}

pub enum Msg {

}

#[derive(PartialEq, Clone)]
pub struct Props {
  pub class: String
}

impl Default for Props {
  fn default() -> Self {
    Props { class: String::from("")  }
  }
}

impl Component for Model {
  type Properties = Props;
  type Message = Msg;

  fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
    let data = js! {
      var container = document.createElement("div");
      container.className = @{props.class};
      var map = L.map(container).setView([51.505, -0.09], 13);
      L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
        attribution: "Map data &copy; <a href='https://www.openstreetmap.org/'>OpenStreetMap</a> contributors",
        maxZoom: 19,
        id: "osm",
      }).addTo(map);
      return {container: container, map: map};
    };
    let map = js! { return @{ &data  }.map;  };
    let container = js! { return @{ &data  }.container;  };

    // Leaflet has a bug when instantiating a map on an element with display: none.
    // When this is done, the tiles will only render sporadically.
    // To fix this, we call the invalidateSize() method to let leaflet recalculate what tiles
    // it should render. Obviously, this method will create problems if view() is not called in the
    // same event loop tick as create().
    js! { @(no_return) setTimeout(function(){ @{&map}.invalidateSize(); }, 0);  };

    Model {
      map: map,
      container: container,
    }
  }

  fn update(&mut self, _msg: Self::Message) -> ShouldRender {
    false
  }
}

impl Renderable<Model> for Model {
  fn view(&self) -> Html<Self> {
    let node = Node::try_from(&(self.container)).expect("convert map js");
    let vnode = VNode::VRef(node);
    vnode
  }
}

trait Layer: TryInto<Value> {
  fn attribution(&self) -> String;
}
