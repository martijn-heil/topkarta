#![feature(custom_attribute)]

#[macro_use]
extern crate stdweb;

#[macro_use]
extern crate stdweb_derive;

#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;

extern crate topkarta_dataworker;
extern crate log;
extern crate simple_logger;
extern crate stdweb_ext;

use log::{trace, debug, info, warn, error};

use stdweb::*;
use stdweb::web::*;
use stdweb::unstable::TryInto;
use stdweb::web::event::IMessageEvent;
use unwrap::unwrap;

use topkarta_dataworker::Request;
use topkarta_dataworker::Response;

use stdweb_ext::*;

fn handle_message(msg: Request) {
  info!("Received message: {:?}", msg);
  match msg {
    Request::Init(name, path, blob) => {
      respond(Response::Ready(()));
    }
  }
}


fn main() {
  unwrap!(simple_logger::init());
  stdweb::initialize();
  assert!(is_worker(), "The Topkarta data worker can only be started as a web worker! {if (window.Worker) was falsy}");
  debug!("We're alive!");
  worker().add_event_listener(move |e: WorkerMessageEvent| { handle_message(unwrap!(e.data().try_into())); });
  js! {
    onmessage = e => console.log("got message");
  }
  debug!("Initial startup is done, entering event loop..");
  stdweb::event_loop();
}

fn is_worker() -> bool {
  true // TODO
}

fn respond(response: Response) {
  js! { @(no_return) postMessage(@{response}); }
}
