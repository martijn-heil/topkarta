#[macro_use]
extern crate stdweb;

#[macro_use]
extern crate stdweb_derive;

#[macro_use]
extern crate serde_derive;
extern crate serde;

use stdweb::Value;

#[derive(Debug, Serialize, Deserialize)]
pub enum Request {
 Init(String, String, Value), // name, path
}
js_serializable!(Request);
js_deserializable!(Request);

#[derive(Debug, Serialize, Deserialize)]
pub enum Response {
 Ready(()),
 Log(String), // message
}
js_serializable!(Response);
js_deserializable!(Response);
